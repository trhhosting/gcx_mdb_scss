#!/bin/bash
woff2_compress Rubik-BlackItalic.ttf
woff2_compress Rubik-Black.ttf
woff2_compress Rubik-BoldItalic.ttf
woff2_compress Rubik-Bold.ttf
woff2_compress Rubik-Italic.ttf
woff2_compress Rubik-LightItalic.ttf
woff2_compress Rubik-Light.ttf
woff2_compress Rubik-MediumItalic.ttf
woff2_compress Rubik-Medium.ttf
woff2_compress Rubik-Regular.ttf
